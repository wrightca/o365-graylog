# Office 365 Graylog Connector

Rudimentary script to pull Office 365 logs and push them into a Syslog TCP input in Graylog 3.

Securely Write Password to File
-------------------------------
(get-credential).password | ConvertFrom-SecureString | set-content "cred.txt"

Install PowerShell
------------------
wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb

sudo apt-get install ./packages-microsoft-prod.deb

sudo apt update

sudo apt install powershell