#########################################################################
#
# PowerShell to pull Office 365 Unified audit logs and send them to 
# a local Graylog server
#
# This is intended to be used within a cron job on the local Graylog
# server.
#
# Stores the events temporarily in a local file, but I'd like to get it
# where it can work completely with variables. I had trouble with type
# and conversions when originally trying to work with only the events
# array variable.
#
# Requires:
# - Username stored in the $username variable
# - Password stored securely in the ./cred.txt file
# - Graylog IP stored in $remotehost variable
# - Graylog Input Port stored in $port variable
#
# Adjust the $minutes variable (how far back from present to get logs) 
# to coincide with the frequency of your cron job.
# 
# Tested in PowerShell on Ubuntu only
#
#########################################################################

# Variables for the connection the O365
$uri = 'https://outlook.office365.com/powershell-liveid/'
$recordtypes = 'AzureActiveDirectory,AzureActiveDirectoryAccountLogon,AzureActiveDirectoryStsLogon,ExchangeAdmin'
$minutes = 60

# Grab credentials and save them for future use
#$credential = Import-CliXml -Path 'o365cred.xml'
#$credential | Export-CliXml -Path 'o365cred.xml'
$username = '[Office 365 Admin Account Username]'
$password = get-content cred.txt | convertto-securestring
$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username,$password

# Variables for the transmission of the logs to the Graylog server
$port = 12201
$remoteHost = '[Graylog IP Address]'

#$out_path = "C:\O365AuditLogs\log_$((Get-Date).ToString('MM-dd-yyyy_hh-mm-ss')).csv"
$out_path = "log_$((Get-Date).ToString('MM-dd-yyyy_hh-mm-ss')).csv"

# Create the PowerShell Session to connect and grab the Unified logs
$Session = New-PSSession –ConfigurationName Microsoft.Exchange –ConnectionUri $uri -Credential $credential –Authentication Basic -AllowRedirection
Import-PSSession $Session

# Do the audit log search and send it to a file
Search-UnifiedAuditLog -EndDate (Get-Date) -StartDate (Get-Date).AddMinutes(-([int]$minutes)) -RecordType $recordtypes -ResultSize 5000 | Export-Csv -Path $out_path
# Do the audit log search and send it to a variable
#$audit_output_raw = Search-UnifiedAuditLog -EndDate (Get-Date) -StartDate (Get-Date).AddMinutes(-([int]$minutes)) -RecordType $recordtypes -ResultSize 5000
# Do the audit log search and output it to screen
#Search-UnifiedAuditLog -EndDate (Get-Date) -StartDate (Get-Date).AddMinutes(-([int]$minutes)) -RecordType $recordtypes -ResultSize 5000 | Format-List -Property AuditData

# Setup and execute the transmission to the Graylog server
$audit_output_raw = Get-Content -Path $out_path | Select-Object -Skip 1

# Convert the input to strings and properly format the object with EOL characters
$ofs = "`n"
$audit_output = [string]$audit_output_raw 

#Write-Output $audit_output
$socket = new-object System.Net.Sockets.TcpClient($remoteHost, $port)
$data = [System.Text.Encoding]::ASCII.GetBytes($audit_output)
$stream = $socket.GetStream()
$stream.Write($data, 0, $data.Length)

# Remove all PS Sessions so that we don't clog up the pipes
Get-PSSession | Remove-PSSession

# Delete the output file
Remove-Item -path $out_path
